import React, { lazy, Suspense } from "react";
import { Redirect } from "react-router";

import CalendarIcon from "@material-ui/icons/CalendarToday";
import TableIcon from "@material-ui/icons/TableChartOutlined";

const TableView = lazy(() => import("./TableView"));
const CalendarView = lazy(() => import("./CalendarView"));

export const navRoutes = [
  {
    path: "/calendar",
    component: () => (
      <Suspense fallback={null}>
        <CalendarView />
      </Suspense>
    ),
    title: "Calendar View",
    Icon: CalendarIcon
  },
  {
    path: "/table",
    exact: true,
    component: () => (
      <Suspense fallback={null}>
        <TableView />
      </Suspense>
    ),
    title: "Table View",
    Icon: TableIcon
  }
];

export default [
  {
    path: "/",
    exact: true,
    component: () => <Redirect to="/calendar" />
  },
  ...navRoutes
];
