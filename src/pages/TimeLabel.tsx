import React from "react";

import moment from "moment";

const TimeLabel = ({ date }: { date: Date }) => {
  const formatted = moment(date).format("dddd, MMMM Do YYYY, h:mm:ss a");
  return <>{formatted}</>;
};

export default TimeLabel;
