import { useMemo } from "react";
import { useSelector } from "react-redux";

import { selectors as employeeSelectors } from "@/modules/employee";
import { selectors as roleSelectors } from "@/modules/role";
import { selectors as shiftSelectors } from "@/modules/shift";
import { selectors as configSelectors } from "@/modules/config";

import moment from "moment";
import "moment-timezone";

const useTableData = () => {
  const getEmployeeById = useSelector(employeeSelectors.getEmployeeById);
  const getRoleById = useSelector(roleSelectors.getRoleById);
  const getShiftById = useSelector(shiftSelectors.getShiftById);
  const allShiftIds = useSelector(shiftSelectors.getAllShiftIds);
  const timezone = useSelector(configSelectors.getTimezone);

  return {
    rows: useMemo(() => {
      return allShiftIds
        .map(shiftId => {
          return getShiftById(shiftId)!;
        })
        .map(shift => {
          const {
            employee_id,
            role_id,
            start_time,
            end_time,
            ...otherShiftData
          } = shift;

          return {
            employee: getEmployeeById(employee_id)!,
            role: getRoleById(role_id)!,
            startTime: moment.tz(start_time, timezone).toDate(),
            endTime: moment.tz(end_time, timezone).toDate(),
            ...otherShiftData
          };
        });
    }, [timezone, getEmployeeById, getRoleById, getShiftById, allShiftIds])
  };
};

export default useTableData;
