import React from "react";
import { render } from "react-testing-library";
import { Provider } from "react-redux";
import { createStore } from "redux";

import useCalendarData from "../useCalendarData";
import state from "./__mocks__/mockState.json";

test("should render data", () => {
  const store = createStore(s => s, state);

  const App = () => {
    const data = useCalendarData();
    return <>{JSON.stringify(data, null, 1)}</>;
  };

  const { container } = render(
    <Provider store={store}>
      <App />
    </Provider>
  );

  expect(container).toMatchSnapshot();
});
