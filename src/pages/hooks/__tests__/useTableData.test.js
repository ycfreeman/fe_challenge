import React from "react";
import { render } from "react-testing-library";
import { Provider } from "react-redux";
import { createStore } from "redux";

import useTableData from "../useTableData";
import state from "./__mocks__/mockState.json";

test("should render data", () => {
  const store = createStore(s => s, state);

  const App = () => {
    const data = useTableData();
    return <>{JSON.stringify(data, null, 1)}</>;
  };

  const { container } = render(
    <Provider store={store}>
      <App />
    </Provider>
  );

  expect(container).toMatchSnapshot();
});
