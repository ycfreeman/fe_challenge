import { useMemo } from "react";
import useTableData from "./useTableData";
import { Event } from "react-big-calendar";

const useCalendarData = () => {
  const { rows } = useTableData();

  return {
    events: useMemo(() => {
      return rows.map(
        (row): Event => {
          return {
            id: row.id,
            title: `${row.employee.first_name} ${row.employee.last_name}`,
            desc: row.role.name,
            start: row.startTime,
            end: row.endTime,
            background_colour: row.role.background_colour,
            text_colour: row.role.text_colour
          };
        }
      );
    }, [rows])
  };
};

export default useCalendarData;
