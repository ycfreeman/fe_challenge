import React, { lazy, Suspense } from "react";

import { createStyles, Theme, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

import useTableData from "./hooks/useTableData";

const TimeLabel = lazy(() => import("./TimeLabel"));

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: "100%",
      marginTop: theme.spacing(3),
      overflowX: "auto"
    },
    table: {
      minWidth: 650
    }
  })
);

// TODO: let's just leave the sorting stuff for now

const SimpleTable = () => {
  const classes = useStyles();

  const { rows } = useTableData();

  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Employee</TableCell>
            <TableCell>Role</TableCell>
            <TableCell>Start Time</TableCell>
            <TableCell>End Time</TableCell>
            <TableCell>Break Duration</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map(row => (
            <TableRow
              key={row.id}
              style={{
                background: row.role.background_colour,
                color: row.role.text_colour
              }}
            >
              <TableCell component="th" scope="row">
                {`${row.employee.first_name} ${row.employee.last_name}`}
              </TableCell>
              <TableCell>{row.role.name}</TableCell>
              <TableCell>
                <Suspense fallback={null}>
                  <TimeLabel date={row.startTime} />
                </Suspense>
              </TableCell>
              <TableCell>
                <Suspense fallback={null}>
                  <TimeLabel date={row.endTime} />
                </Suspense>
              </TableCell>
              <TableCell>{row.break_duration}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>
  );
};

export default SimpleTable;
