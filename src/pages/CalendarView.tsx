import React from "react";
import BigCalendar from "react-big-calendar";
import moment from "moment";
import "moment-timezone";

import withDragAndDrop from "react-big-calendar/lib/addons/dragAndDrop";
// TODO: mobile drag n drop doesn't work apparently

import { useDispatch } from "react-redux";

import "react-big-calendar/lib/addons/dragAndDrop/styles.css";
import "react-big-calendar/lib/css/react-big-calendar.css";

import useCalendarData from "./hooks/useCalendarData";

import { actions as shiftActions } from "@/modules/shift";

const DnDCalendar = withDragAndDrop(BigCalendar);

const NOW = new Date(2018, 5, 18); // TODO: just set "today" to 18/june/18 for simplicity for this exercise

const EventView: React.FC<{ event: any }> = ({ event }) => {
  // TODO: fix this colouring issue and types

  return (
    <div
      style={{ background: event.background_colour, color: event.text_colour }}
    >
      <strong>{event.title}</strong>
      {event.desc && ":  " + event.desc}
    </div>
  );
};

const components: any = {
  event: EventView
};

const CalendarView = () => {
  const localizer = BigCalendar.momentLocalizer(moment);

  const { events } = useCalendarData();
  const dispatch = useDispatch();

  const updateShift = (id: number, start: Date, end: Date) => {
    return dispatch(shiftActions.updateShift(id, start, end));
  };

  const onEventResize = ({ event, start, end }: any) => {
    updateShift(event.id, start, end);
  };

  const onEventDrop = ({ event, start, end }: any) => {
    updateShift(event.id, start, end);
  };
  return (
    <DnDCalendar
      components={components}
      getNow={() => NOW}
      localizer={localizer}
      resizable={true}
      views={[
        BigCalendar.Views.MONTH,
        BigCalendar.Views.WEEK,
        BigCalendar.Views.DAY
      ]}
      events={events}
      onEventDrop={onEventDrop}
      onEventResize={onEventResize}
      defaultView={BigCalendar.Views.DAY}
    />
  );
};

export default CalendarView;
