import React, { useEffect } from "react";
import { BrowserRouter } from "react-router-dom";
import { renderRoutes } from "react-router-config";
import { useDispatch } from "react-redux";

import { actions as configActions } from "@/modules/config";
import { actions as employeeActions } from "@/modules/employee";
import { actions as roleActions } from "@/modules/role";
import { actions as shiftActions } from "@/modules/shift";

import routes from "./routes";
import Navigation from "./Navigation";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    // just fetch all fake data here
    // TODO: it would be a good idea to tie these to a store switch dropdown or something
    dispatch(configActions.fetchConfig());
    dispatch(employeeActions.fetchEmployees());
    dispatch(roleActions.fetchRoles());
    dispatch(shiftActions.fetchShifts());
  }, [dispatch]);

  return (
    <BrowserRouter>
      <Navigation>{renderRoutes(routes)}</Navigation>
    </BrowserRouter>
  );
};

export default App;
