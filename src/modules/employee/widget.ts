import { createAction, handleActions, Action } from "redux-actions";
import { AnyAction } from "redux";

import rawJSON from "@/files/employees.json";
import { keyBy, get } from "lodash";

// types
interface Employee {
  last_name: string;
  first_name: string;
  id: number;
}

type EmployeeMap = { [key: string]: Employee };

type StateShape = EmployeeMap;

export const DOMAIN = "employee";

const initalState: StateShape = {};

// actions
const fetchEmployees = () =>
  ((async (dispatch: any) => {
    return dispatch(setEmployees(keyBy(rawJSON, "id")));
  }) as any) as AnyAction;

const setEmployees = createAction(
  `${DOMAIN}/setEmployees`,
  (v: EmployeeMap) => v
);

// reducers
const reducer = handleActions(
  {
    [setEmployees.toString()](s, a: Action<EmployeeMap>) {
      return {
        ...a.payload
      };
    }
  },
  initalState
);

// selectors
const getScopedState = (s: any) => s[DOMAIN] as StateShape;
const getEmployeeById = (s: any) => (id: number) =>
  get(getScopedState(s), id, undefined);

export const actions = {
  fetchEmployees
};
export const reducers = reducer;
export const selectors = { getAllEmployees: getScopedState, getEmployeeById };
