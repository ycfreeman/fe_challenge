import { createAction, handleActions, Action } from "redux-actions";
import { AnyAction } from "redux";

import rawJSON from "@/files/config.json";

// types
interface Config {
  timezone: string;
  location: string;
}

interface StateShape extends Config {}

export const DOMAIN = "config";

const initalState: StateShape = {
  timezone: "",
  location: ""
};

// actions
const fetchConfig = () =>
  ((async (dispatch: any) => {
    return dispatch(setConfig(rawJSON));
  }) as any) as AnyAction;

const setConfig = createAction(`${DOMAIN}/setConfig`, (v: Config) => v);

// reducers
const reducer = handleActions(
  {
    [setConfig.toString()](s, a: Action<Config>) {
      return {
        ...a.payload
      };
    }
  },
  initalState
);

// selectors
const getScopedState = (s: any) => s[DOMAIN] as StateShape;

const getLocation = (s: any) => getScopedState(s).location;
const getTimezone = (s: any) => getScopedState(s).timezone;

export const actions = {
  fetchConfig
};
export const reducers = reducer;
export const selectors = { getLocation, getTimezone };
