import React from "react";
import { useSelector } from "react-redux";
import { selectors } from "./widget";

const AppTitle = () => {
  const title = useSelector(selectors.getLocation);
  return <>{title}</>;
};

export default AppTitle;
