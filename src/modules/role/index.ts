import { DOMAIN, actions, reducers, selectors } from "./widget";
import { installReducer } from "@/core/store";

installReducer(DOMAIN, reducers);

export { actions, selectors };
