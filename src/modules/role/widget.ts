import { createAction, handleActions, Action } from "redux-actions";
import { AnyAction } from "redux";

import rawJSON from "@/files/roles.json";
import { keyBy, get } from "lodash";

// types
interface Role {
  background_colour: string;
  name: string;
  id: number;
  text_colour: string;
}

type RoleMap = { [key: string]: Role };

interface StateShape extends RoleMap {}

export const DOMAIN = "role";

const initalState: StateShape = {};

// actions
const fetchRoles = () =>
  ((async (dispatch: any) => {
    return dispatch(setRoles(keyBy(rawJSON, "id")));
  }) as any) as AnyAction;

const setRoles = createAction(`${DOMAIN}/setRoles`, (v: RoleMap) => v);

// reducers
const reducer = handleActions(
  {
    [setRoles.toString()](s, a: Action<RoleMap>) {
      return {
        ...a.payload
      };
    }
  },
  initalState
);

// selectors
const getScopedState = (s: any) => s[DOMAIN] as StateShape;
const getRoleById = (s: any) => (id: number) =>
  get(getScopedState(s), id, undefined);

export const actions = {
  fetchRoles
};
export const reducers = reducer;
export const selectors = { getAllRoles: getScopedState, getRoleById };
