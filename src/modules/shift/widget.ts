import { createAction, handleActions, Action } from "redux-actions";
import { AnyAction } from "redux";

import rawJSON from "@/files/shifts.json";
import { keyBy, get, map, mapValues, set } from "lodash";
import moment from "moment";

// types
interface Shift {
  employee_id: number;
  start_time: Date;
  role_id: number;
  end_time: Date;
  id: number;
  break_duration: number;
}

type ShiftMap = { [key: string]: Shift };

interface StateShape extends ShiftMap {}

export const DOMAIN = "shift";

const initalState: StateShape = {};

// actions
const fetchShifts = () =>
  ((async (dispatch: any) => {
    const processedJSON = mapValues(keyBy(rawJSON, "id"), v => {
      return {
        ...v,
        start_time: moment.utc(v.start_time).toDate(),
        end_time: moment.utc(v.end_time).toDate()
      };
    });
    return dispatch(setShifts(processedJSON));
  }) as any) as AnyAction;

const setShifts = createAction(`${DOMAIN}/setShifts`, (v: ShiftMap) => v);

const updateShift = createAction(
  // we can also do a thunk here to send things back to server
  `${DOMAIN}/updateShift`,
  (id: number, start_time: Date, end_time: Date) => ({
    id,
    start_time,
    end_time
  })
);

// reducers
const reducer = handleActions(
  {
    [setShifts.toString()](s, a: Action<ShiftMap>) {
      return {
        ...a.payload
      };
    },
    [updateShift.toString()](s, a: Action<any>) {
      let newState = {
        ...s
      };
      newState = set(
        newState,
        [a.payload.id, "start_time"],
        a.payload.start_time
      );
      newState = set(newState, [a.payload.id, "end_time"], a.payload.end_time);

      return newState;
    }
  },
  initalState
);

// selectors
const getScopedState = (s: any) => s[DOMAIN] as StateShape;
const getAllShiftIds = (s: any) => map(getScopedState(s), "id");
const getShiftById = (s: any) => (id: number) =>
  get(getScopedState(s), id, undefined);

export const actions = {
  fetchShifts,
  updateShift
};
export const reducers = reducer;
export const selectors = {
  getAllShifts: getScopedState,
  getShiftById,
  getAllShiftIds
};
