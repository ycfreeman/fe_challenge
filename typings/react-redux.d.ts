import "react-redux";
import { Dispatch } from "redux";
declare module "react-redux" {
  export const useSelector: <T>(selector: (state: any) => T, deps?: any[]) => T;
  export const useActions: any;
  export const useDispatch: () => Dispatch;
}
