### getting started

```
yarn
yarn start
```

### notes

- this is achived with pure client side code
- some non production ready points are documented as TODOs
- typescript is used to ensure correctness
- due to limited time is spent, test coverage is only limited to significant data transformation functions and hooks

### credits/ tech used

- typescript
- react, create-react-app(typescript)
- redux, react-redux
- react-router, use-react-router
- https://github.com/ycfreeman/modular-redux-playground
- https://github.com/intljusticemission/react-big-calendar
- https://next.material-ui.com/
- moment, moment-timezone
